$(document).ready(function($) {

    $(window).on('scroll', function() {

        if ($(window).scrollTop() + $(window).height() > $('#container').outerHeight()) {
            $('body').addClass('tight');
            $('.arrow').hide();
        } else {
            $('body').removeClass('tight');
            $('.arrow').show();
        }
    });

    $("html").on("click", "body.tight #container", function() {
        $('html, body').animate({
            scrollTop: $('#container').outerHeight() - $(window).height()
        }, 500);
    });

});

$('.arrow').click(function() {
    $("html").animate({ scrollTop: $('html').prop("scrollHeight") }, 1200);
});